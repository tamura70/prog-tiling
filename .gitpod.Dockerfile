FROM gitpod/workspace-full

# sbt
RUN curl -LO https://github.com/sbt/sbt/releases/download/v1.4.3/sbt-1.4.3.zip \
    && unzip sbt-1.4.3.zip
ENV PATH $PATH:/home/gitpod/sbt/bin
