import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Piece implements Comparable<Piece> {
    public int i0 = 0;
    public int j0 = 0;
    public Shape shape0;
    public List<Shape> shapes;

    public Piece(int[][] area, int v, boolean rotate, boolean flip) {
        initPos(area, v);
        shapes = new ArrayList<Shape>();
        shape0 = new Shape(area, v);
        shapes.add(shape0);
        for (int f = 0; f <= (flip ? 1 : 0); f++) {
            Shape shape = (f == 0) ? shape0 : shape0.flip();
            for (int rot = 0; rot <= (rotate ? 3 : 0); rot++) {
                if (!shapes.contains(shape))
                    shapes.add(shape);
                shape = shape.rotate();
            }
        }
        Collections.sort(shapes);
    }

    private void initPos(int[][] area, int v) {
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[i].length; j++) {
                if (area[i][j] == v) {
                    i0 = i;
                    j0 = j;
                    return;
                }
            }
        }
    }

    public int size() {
        return shape0.positions.length;
    }

    public int compareTo(Piece piece) {
        if (shapes.size() != piece.shapes.size())
            return shapes.size() < piece.shapes.size() ? -1 : 1;
        for (int k = 0; k < shapes.size(); k++) {
            int cmp = shapes.get(k).compareTo(piece.shapes.get(k));
            if (cmp != 0)
                return cmp;
        }
        return 0;
    }

    public int hashCode() {
        return shapes.hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Piece))
            return false;
        return shapes.equals(((Piece) o).shapes);
    }

    public String toString() {
        String s = "";
        String delim = "";
        for (int i = 0; i < shapes.size(); i++) {
            s += delim + shapes.get(i).toString();
            delim = ",\n";
        }
        return "Piece(" + i0 + "," + j0 + "," + shape0 + ",{\n" + s + "\n})";
    }
}
