public class Position implements Comparable<Position> {
    public int i;
    public int j;

    public Position(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public int compareTo(Position position) {
        if (i != position.i)
            return i < position.i ? -1 : 1;
        if (j != position.j)
            return j < position.j ? -1 : 1;
        return 0;
    }

    public int hashCode() {
        return 13 * i + j;
    }

    public boolean equals(Object o) {
        if (!(o instanceof Position))
            return false;
        Position position = (Position) o;
        return compareTo(position) == 0;
    }

    public String toString() {
        return "(" + i + "," + j + ")";
    }

}
