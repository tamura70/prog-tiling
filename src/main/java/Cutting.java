import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Cutting implements Comparable<Cutting> {
    private List<Piece> cutted;
    private List<Piece> remaining;

    public Cutting(List<Piece> pieces, Area area) {
        cutted = new ArrayList<Piece>(pieces);
        Collections.sort(cutted);
        remaining = area.remainingPieces();
        Collections.sort(remaining);
    }

    public int compareTo(Cutting cutting) {
        if (cutted.size() != cutting.cutted.size())
            return cutted.size() < cutting.cutted.size() ? -1 : 1;
        for (int i = 0; i < cutted.size(); i++) {
            int cmp = cutted.get(i).compareTo(cutting.cutted.get(i));
            if (cmp != 0)
                return cmp;
        }
        if (remaining.size() != cutting.remaining.size())
            return remaining.size() < cutting.remaining.size() ? -1 : 1;
        for (int i = 0; i < remaining.size(); i++) {
            int cmp = remaining.get(i).compareTo(cutting.remaining.get(i));
            if (cmp != 0)
                return cmp;
        }
        return 0;
    }

    public int hashCode() {
        return 3 * cutted.hashCode() + remaining.hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Cutting))
            return false;
        return compareTo((Cutting) o) == 0;
    }

    public String toString() {
        String s1 = "";
        String delim = "";
        for (int i = 0; i < cutted.size(); i++) {
            s1 += delim + cutted.get(i).toString();
            delim = ",\n";
        }
        String s2 = "";
        delim = "";
        for (int i = 0; i < remaining.size(); i++) {
            s2 += delim + remaining.get(i).toString();
            delim = ",\n";
        }
        return "Cutting({\n" + s1 + "\n},{\n" + s2 + "\n})";
    }

}
