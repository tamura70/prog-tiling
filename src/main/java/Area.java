import java.util.ArrayList;
import java.util.List;

public class Area {
    public int[][] area;
    private int m;
    private int n;
    private int size;
    private int remainingSize;
    public int pieceCount;
    private Area target;
    private boolean rotate;
    private boolean flip;

    public Area(int[][] a, boolean rotate, boolean flip) {
        m = a.length;
        n = a[0].length;
        area = new int[m][n];
        size = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                area[i][j] = 0;
                if (a[i][j] != 0) {
                    area[i][j] = -1;
                    size++;
                }
            }
        }
        remainingSize = size;
        pieceCount = 0;
        target = null;
        this.rotate = rotate;
        this.flip = flip;
    }

    public Area(String[] a, boolean rotate, boolean flip) {
        m = a.length;
        n = 0;
        for (int i = 0; i < m; i++) {
            n = Math.max(n, a[i].length());
        }
        area = new int[m][n];
        size = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                area[i][j] = 0;
                if (j < a[i].length() && a[i].charAt(j) != ' ') {
                    area[i][j] = -1;
                    size++;
                }
            }
        }
        remainingSize = size;
        pieceCount = 0;
        target = null;
        this.rotate = rotate;
        this.flip = flip;
    }

    public int size() {
        return size;
    }

    public int remainingSize() {
        return remainingSize;
    }

    public void setTarget(Area a) {
        target = a;
    }

    public Area target() {
        return target;
    }

    public int maxI() {
        return m;
    }

    public int maxJ() {
        return area[0].length;
    }

    public boolean remaining(int i, int j) {
        return area[i][j] < 0;
    }

    public boolean canPlace(Shape shape, int i0, int j0) {
        for (int k = shape.positions.length - 1; k >= 0; k--) {
            int i = i0 + shape.positions[k].i;
            int j = j0 + shape.positions[k].j;
            if (i < 0 || m <= i ||
                    j < 0 || n <= j || area[i][j] >= 0)
                return false;
        }
        return true;
    }

    private void place0(Shape shape, int i0, int j0, int num) {
        for (int k = 0; k < shape.positions.length; k++) {
            int i = i0 + shape.positions[k].i;
            int j = j0 + shape.positions[k].j;
            area[i][j] = num;
        }
    }

    public void place(Shape shape, int i0, int j0, int num) {
        place0(shape, i0, j0, num);
        remainingSize -= shape.positions.length;
        pieceCount++;
    }

    public void unPlace(Shape shape, int i0, int j0) {
        place0(shape, i0, j0, -1);
        remainingSize += shape.positions.length;
        pieceCount--;
    }

    public boolean canCut(Piece piece) {
        return canPlace(piece.shape0, piece.i0, piece.j0);
    }

    public void cut(Piece piece, int num) {
        place0(piece.shape0, piece.i0, piece.j0, num);
        remainingSize -= piece.shape0.positions.length;
        pieceCount++;
    }

    public void unCut(Piece piece) {
        place0(piece.shape0, piece.i0, piece.j0, -1);
        remainingSize += piece.shape0.positions.length;
        pieceCount--;
    }

    private void normalize() {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (area[i][j] < 0)
                    area[i][j] = -1;
            }
        }
    }

    private int paintConnected(int i, int j, int num) {
        int c = 0;
        if (0 <= i && i < m &&
                0 <= j && j < n && area[i][j] == -1) {
            area[i][j] = num;
            c++;
            c += paintConnected(i - 1, j, num);
            c += paintConnected(i, j - 1, num);
            c += paintConnected(i, j + 1, num);
            c += paintConnected(i + 1, j, num);
        }
        return c;
    }

    private int minimumPieceCount(int num, int pieceSize) {
        int c = pieceCount;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (area[i][j] == -1) {
                    int size = paintConnected(i, j, num);
                    c += (size + pieceSize - 1) / pieceSize;
                    num--;
                }
            }
        }
        return c;
    }

    public int minimumPieceCount(int pieceSize) {
        normalize();
        int count = minimumPieceCount(-2, pieceSize);
        normalize();
        return count;
    }

    public int remainingPieceCount() {
        return minimumPieceCount(size());
    }

    private void addRemainingPieces(List<Piece> list, int num) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (area[i][j] == -1) {
                    paintConnected(i, j, num);
                    Piece piece = new Piece(area, num, rotate, flip);
                    list.add(piece);
                    num--;
                }
            }
        }
    }

    public List<Piece> remainingPieces() {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (area[i][j] < 0)
                    area[i][j] = -1;
            }
        }
        List<Piece> list = new ArrayList<Piece>();
        addRemainingPieces(list, -2);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (area[i][j] < 0)
                    area[i][j] = -1;
            }
        }
        return list;
    }

    private boolean connected(int i, int j) {
        return 0 <= i && i < m &&
                0 <= j && j < n && area[i][j] == -2;
    }

    private boolean canPlaceToTarget(Piece piece) {
        for (int i = 0; i < target.m; i++) {
            for (int j = 0; j < target.n; j++) {
                if (target.area[i][j] >= 0)
                    continue;
                for (int k = 0; k < piece.shapes.size(); k++) {
                    if (target.canPlace(piece.shapes.get(k), i, j)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void addPieces(List<Piece> list, int i0, int j0, int pieceSize) {
        if (pieceSize == 0) {
            Piece piece = new Piece(area, -2, rotate, flip);
            if (canPlaceToTarget(piece))
                list.add(piece);
            return;
        }
        for (int i = i0; i < m; i++) {
            for (int j = (i == i0) ? j0 + 1 : 0; j < n; j++) {
                if (area[i][j] != -1)
                    continue;
                if (connected(i - 1, j) || connected(i, j - 1) ||
                        connected(i, j + 1) || connected(i + 1, j)) {
                    area[i][j] = -2;
                    addPieces(list, i0, j0, pieceSize - 1);
                    area[i][j] = -3;
                    addPieces(list, i0, j0, pieceSize);
                    area[i][j] = -1;
                    return;
                }
            }
        }
    }

    public List<Piece> pieceList(int pieceSize) {
        normalize();
        List<Piece> list = new ArrayList<Piece>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (area[i][j] == -1) {
                    area[i][j] = -2;
                    addPieces(list, i, j, pieceSize - 1);
                    area[i][j] = -1;
                }
            }
        }
        normalize();
        return list;
    }

    public String toString() {
        String s = "";
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                char c = '.';
                if (area[i][j] < 0) {
                    c = '*';
                } else if (area[i][j] > 0) {
                    c = (char) (area[i][j] - 1);
                    while (true) {
                        c = (char) (c + 'A');
                        if (c <= 'Z')
                            break;
                        c = (char) (c - 'Z' - 1 + 'a');
                        if (c <= 'z')
                            break;
                        c = (char) (c - 'z' - 1);
                    }
                }
                s += c;
            }
            s += "\n";
        }
        return s;
    }

}
