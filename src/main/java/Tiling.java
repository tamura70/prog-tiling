import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/*
 * Polyomino Dissection and Tiling Solver
 * by Naoyuki Tamura
 * Jan. 2005
 */
public class Tiling {
    private static int maxPieceCount;
    private static Area area0;
    private static Area area1;
    private static int count = 0;
    private static long time0;
    private static Stack<Piece> cuttedPieces;
    private static Set<Cutting> cuttings;
    private static int level;

    private static String timeString(long time) {
        int secs = (int) (time / 1000);
        int h = secs / 3600;
        int m = (secs / 60) % 60;
        int s = secs % 60;
        String str = "";
        if (h < 10)
            str += "0";
        str += h + ":";
        if (m < 10)
            str += "0";
        str += m + ":";
        if (s < 10)
            str += "0";
        str += s;
        return str;
    }

    private static void showSolution() {
        count++;
        long time = System.currentTimeMillis() - time0;
        String t = timeString(time);
        System.out.print("##### Solution " + count);
        System.out.print(" pieces=" + cuttedPieces.size());
        System.out.print(" time=" + t);
        System.out.println();
        System.out.println(area0);
        System.out.println(area1);
    }

    private static void showStatus() {
        long time = System.currentTimeMillis() - time0;
        String t = timeString(time);
        System.out.print("##### Status");
        System.out.print(" found=" + count);
        System.out.print(" pieces=" + maxPieceCount);
        System.out.print(" time=" + t);
        System.out.println();
        System.out.println(area0);
        // System.out.println(area1);
    }

    private static void solve(int pieceSize, List<Piece> pieces, int index) {
        int number = cuttedPieces.size();
        if (number > maxPieceCount)
            return;
        if (area0.minimumPieceCount(pieceSize) > maxPieceCount)
            return;
        if (area1.minimumPieceCount(pieceSize) > maxPieceCount)
            return;
        if (area0.remainingSize() == 0) {
            showSolution();
            return;
        }
        for (; index < pieces.size(); index++) {
            Piece piece = pieces.get(index);
            if (!area0.canCut(piece))
                continue;
            area0.cut(piece, number + 1);
            cuttedPieces.push(piece);
            if (number + 1 <= level) {
                Cutting cutting = new Cutting(cuttedPieces, area0);
                if (cuttings.contains(cutting)) {
                    cuttedPieces.pop();
                    area0.unCut(piece);
                    continue;
                }
                cuttings.add(cutting);
                showStatus();
            }
            for (int i = 0; i < area1.maxI(); i++) {
                for (int j = 0; j < area1.maxJ(); j++) {
                    if (!area1.remaining(i, j))
                        continue;
                    for (int k = 0; k < piece.shapes.size(); k++) {
                        Shape shape = piece.shapes.get(k);
                        if (area1.canPlace(shape, i, j)) {
                            area1.place(shape, i, j, number + 1);
                            solve(pieceSize, pieces, index + 1);
                            area1.unPlace(shape, i, j);
                        }
                    }
                }
            }
            cuttedPieces.pop();
            area0.unCut(piece);
        }
        solve(pieceSize - 1);
    }

    private static void solve(int pieceSize) {
        if (pieceSize <= 0)
            return;
        List<Piece> pieces = area0.pieceList(pieceSize);
        solve(pieceSize, pieces, 0);
    }

    private static void solve(Area a0, Area a1, int maxPiece, int level0) {
        if (a0.size() != a1.size())
            return;
        if (a0.size() == 0)
            return;
        if (a0.remainingPieceCount() >= a1.remainingPieceCount()) {
            area0 = a0;
            area1 = a1;
        } else {
            area0 = a1;
            area1 = a0;
        }
        area0.setTarget(area1);
        level = level0;
        System.out.println(new Date());
        time0 = System.currentTimeMillis();
        count = 0;
        maxPieceCount = maxPiece;
        if (maxPieceCount <= 0)
            maxPieceCount = a0.remainingPieceCount();
        while (count == 0 && maxPieceCount <= area0.size()) {
            System.out.println("##### Pieces " + maxPieceCount);
            cuttedPieces = new Stack<Piece>();
            cuttings = new HashSet<Cutting>();
            solve(area1.size());
            maxPieceCount++;
        }
        System.out.println(new Date());
    }

    private static String[][] load(String fileName) throws IOException {
        BufferedReader rd = new BufferedReader(new FileReader(fileName));
        String[] p = {"", ""};
        int i = 0;
        while (true) {
            String line = rd.readLine();
            if (line == null)
                break;
            line = line.trim();
            if (line.startsWith(";"))
                continue;
            if (line.equals("")) {
                i++;
                continue;
            }
            p[i] += line + "\n";
        }
        rd.close();
        p[0] = p[0].replaceAll("#", "*").replaceAll("\\.", " ");
        p[1] = p[1].replaceAll("#", "*").replaceAll("\\.", " ");
        String[][] puzzle = {p[0].split("\n"), p[1].split("\n")};
        return puzzle;
    }

    public static void main(String[] args) {
        int maxPiece = 0;
        int level = 1;
        boolean rotate = true;
        boolean flip = false;
        int i = 0;
        while (i < args.length) {
            if (args[i].startsWith("-max=")) {
                maxPiece = Integer.parseInt(args[i].replace("-max=", ""));
                i++;
            } else if (args[i].startsWith("-level=")) {
                level = Integer.parseInt(args[i].replace("-level=", ""));
                i++;
            } else if (args[i].startsWith("-rotate=")) {
                rotate = (!args[i].replace("-rotate=", "").equals("0"));
                i++;
            } else if (args[i].startsWith("-flip=")) {
                rotate = (!args[i].replace("-flip=", "").equals("0"));
                i++;
            } else {
                break;
            }
        }
        String[][] problem = null;
        try {
            if (i >= args.length)
                throw new RuntimeException("Usage: java -jar tiling.jar file");
            problem = load(args[i]);
        } catch (Exception e) {
            System.out.println(e);
            System.exit(1);
        }
        Area a0 = new Area(problem[0], rotate, flip);
        Area a1 = new Area(problem[1], rotate, flip);
        solve(a0, a1, maxPiece, level);
    }
}
