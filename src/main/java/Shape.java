import java.util.Arrays;

public class Shape implements Comparable<Shape> {
    public Position[] positions;

    private Shape(Position[] ps) {
        positions = new Position[ps.length];
        for (int k = 0; k < ps.length; k++) {
            positions[k] = new Position(ps[k].i, ps[k].j);
        }
    }

    public Shape(int[][] area, int v) {
        int size = 0;
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[i].length; j++) {
                if (area[i][j] == v)
                    size++;
            }
        }
        positions = new Position[size];
        int k = 0;
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[i].length; j++) {
                if (area[i][j] == v)
                    positions[k++] = new Position(i, j);
            }
        }
        normalize();
    }

    private void normalize() {
        Arrays.sort(positions);
        if (positions.length == 0)
            return;
        int i0 = positions[0].i;
        int j0 = positions[0].j;
        for (int k = 0; k < positions.length; k++) {
            positions[k].i -= i0;
            positions[k].j -= j0;
        }
    }

    public Shape rotate() {
        Shape shape = new Shape(positions);
        for (int k = 0; k < positions.length; k++) {
            shape.positions[k].i = -positions[k].j;
            shape.positions[k].j = positions[k].i;
        }
        shape.normalize();
        return shape;
    }

    public Shape flip() {
        Shape shape = new Shape(positions);
        for (int k = 0; k < positions.length; k++) {
            shape.positions[k].j = -positions[k].j;
        }
        shape.normalize();
        return shape;
    }

    public int compareTo(Shape shape) {
        if (positions.length != shape.positions.length)
            return positions.length < shape.positions.length ? -1 : 1;
        for (int k = 0; k < positions.length; k++) {
            int cmp = positions[k].compareTo(shape.positions[k]);
            if (cmp != 0)
                return cmp;
        }
        return 0;
    }

    public int hashCode() {
        int code = 0;
        for (int k = 0; k < positions.length; k++) {
            code = 13 * code + positions[k].hashCode();
        }
        return code;
    }

    public boolean equals(Object o) {
        if (!(o instanceof Shape))
            return false;
        Shape shape = (Shape) o;
        return compareTo(shape) == 0;
    }

    public String toString() {
        String s = "";
        String delim = "";
        for (int k = 0; k < positions.length; k++) {
            s += delim + positions[k];
            delim = ",";
        }
        return "Shape({" + s + "})";
    }

}
